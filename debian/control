Source: libcrypt-twofish-perl
Maintainer: Debian Perl Group <pkg-perl-maintainers@lists.alioth.debian.org>
Uploaders: Damyan Ivanov <dmn@debian.org>,
           Xavier Guimard <yadd@debian.org>
Section: perl
Testsuite: autopkgtest-pkg-perl
Priority: optional
Build-Depends: debhelper-compat (= 13),
               libcrypt-cbc-perl <!nocheck>,
               perl-xs-dev,
               perl:native
Standards-Version: 4.6.1
Vcs-Browser: https://salsa.debian.org/perl-team/modules/packages/libcrypt-twofish-perl
Vcs-Git: https://salsa.debian.org/perl-team/modules/packages/libcrypt-twofish-perl.git
Homepage: https://metacpan.org/release/Crypt-Twofish
Rules-Requires-Root: no

Package: libcrypt-twofish-perl
Architecture: any
Depends: ${misc:Depends},
         ${perl:Depends},
         ${shlibs:Depends}
Description: Perl module for Twofish Encryption Algorithm
 Crypt::Twofish implements Twofish encryption using an interface
 compatible with Crypt::CBC interface.
 .
 Twofish is a 128-bit symmetric block cipher with a variable length (128, 192,
 or 256-bit) key, developed by Counterpane Labs. It is unpatented and free for
 all uses, as described at <http://www.counterpane.com/twofish.html>.
